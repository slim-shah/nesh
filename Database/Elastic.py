'''
Author: Meet Shah(a.k.a slim_shah)
References:
'''
from elasticsearch import Elasticsearch


class ElasticSearch:
    #default constructor initalizing elastic search object
    def __init__(self):
        # Elasticsearch configuration
        ES_HOST = {"host": "localhost", "port": 9200}
        INDEX_NAME = ['transcripts', 'news']

        self.es = Elasticsearch(hosts=[ES_HOST], http_auth=('elastic', 'elastic'))

        request_body = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            }
        }

        #Creating indices if not already crated.
        for i in range(0, len(INDEX_NAME)):
            if not self.es.indices.exists(INDEX_NAME[i]):
                res = self.es.indices.create(index=INDEX_NAME[i], body=request_body)
                print("Index Created", res)
    #End INIT

    '''
    This method ingests data into 'trancripts' index.
    input: company symbol, transcript text to ingest, index name(i.e: transripts), document type(i.e: transcript)
    ouput: status of ingestion.
    '''
    def Ingest(self, sym, content, INDEX_NAME, TYPE_NAME):
        res = self.es.index(index=INDEX_NAME, doc_type=TYPE_NAME, body={
            'company': sym,
            'content': content,
        })
    #End Ingest

    '''
    This method ingests data into 'news' index.
    input: company symbol, article header, date, by, article text.
    ouput: status of ingestion.
    '''
    def Ingest_news(self, article_header, date, by ,article_text):
        res = self.es.index(index='news', doc_type='newss', body={
            'article_header': article_header,
            'date': date,
            'by': by,
            'article_text':article_text
        })
    #End Ingest news


    '''
    This method searches relevant news to company in elastic search.
    input: company symbol, index name(i.e: news)
    ouput: all relevant news to company in form of tuple (id, article header).
    '''
    def Search_news(self, sym, ind):
        res = self.es.search(index=ind, body={
             "size":50,
            "query": {
                "match": {
                    "article_header": sym,
                }
            }
        })
        hits = res['hits']['hits']
        #print(len(hits))
        Headlines = []
        for i in hits:
            #print(i['_id'])
            Headlines.append((i['_id'], i['_source']['article_header']))
        return Headlines
    #End Search

    '''
    This method searches relevant transcript to company in elastic search.
    input: company symbol, index name(i.e: transcripts)
    ouput: all relevant transcripts to company in form of tuple (id, transcript header).
    '''
    def Search_transcript(self, sym, ind):
        res = self.es.search(index=ind, body={
             "size":100,
            "query": {
                "match": {
                    "company": sym,
                }
            }
        })
        hits = res['hits']['hits']
        #print(len(hits))
        Headlines = []
        for i in hits:
            st = i['_source']['content'][:70]
            st = st.replace('\n', ' ')
            print(st)
            Headlines.append((i['_id'], st))
        return Headlines
    #End Search

    '''
    This method fectches detail view of news based on its id.
    input: id of news, index 
    ouput: dictionary of detail specifics of news like(By, Date, Article Text, Article Header)
    '''
    def Detail_News(self, id, ind):
        res = self.es.search(index=ind, body={
            "size": 50,
            "query": {
                "match": {
                    "_id": id,
                }
            }
        })

        result = res['hits']['hits']
        result = result[0]['_source']
        return {'By': result['by'], 'Date':result['date'], 'article_header': result['article_header']\
                , 'article_text':result['article_text']}


    '''
    This method fectches detail view of transcript based on its id.
    input: id of transcript, index 
    ouput: dictionary of detail specifics of transcript like(transcript text)
    '''
    def Detail_Transcript(self, id, ind):
        res = self.es.search(index=ind, body={
            "size": 100,
            "query": {
                "match": {
                    "_id": id,
                }
            }
        })

        result = res['hits']['hits']
        result = result[0]['_source']
        return {'content': result['content']}
    #End Detail_Transcript



'''
for Testing the working Elastic.py

d.Search_news("oxy", "news")
d.Detail_News( "6WkfZmkB1DjPEw1zrLMQ", "news")
d = ElasticSearch()
d.Detail_Transcript("emkeZmkB1DjPEw1zNrMU", "transcripts")
'''

