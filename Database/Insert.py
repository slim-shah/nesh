'''
Author: Meet Shah(a.k.a slim_shah)
References:
    Insert and Fetch Values from database => https://www.youtube.com/watch?v=UsyTSSDMzcI
'''
import mysql.connector

'''
This class is created for storing data into mysql database.
'''
class Populate:
    """
    Default constructor initializing connection object and cursor object.
    """
    def __init__(self):
        #Database Configuration
        self.conn = mysql.connector.connect(user='root', password='root', host='localhost', port='3306', database='nesh')
        self.cursor = self.conn.cursor()
    #End init

    '''
    This method inserts data in company_details of nesh database.
    input: values need to insert in relation company_details.
    ouput: Data inserted into database.
    '''
    def Insert_Database(self, future, market_cap, net_cash_flow, last_year_revenue, sym):

        sql = "select * from company_details where Company_Name = '" + sym + "'"
        self.cursor.execute(sql)

        l = self.cursor.fetchall()
        sql= ''
        if len(l) == 0:
            sql = '''INSERT into company_details(Company_Name, market_cap, net_cash_flow, last_revenue, future) \
                        values("%s", "%s", "%s", "%s", "%s")''' % \
               (sym, market_cap, net_cash_flow, last_year_revenue, future)


        else:
            sql = "UPDATE company_details set market_cap ='" + market_cap + "', net_cash_flow='" + net_cash_flow + "', last_revenue='" \
                    + last_year_revenue +"', future='" + future + "'"

        self.cursor.execute(sql)
        self.cursor.close()
        self.conn.commit()

        print("Inserted in database")
    #End Insert

    '''
    This method is for performing search on database and returining wherever needed.
    input: company symbol
    ouput: dictionary containing all the value in one row of relation 'company_details'.
    '''
    def Search(self, sym):
        sql = "select * from company_details where Company_Name = '" + sym + "'"
        self.cursor.execute(sql)
        Result = self.cursor.fetchall()
        self.cursor.close()
        if len(Result) > 0:
            Res ={ 'sym': Result[0][0], 'marketcap': Result[0][1], 'net_cash_flow':Result[0][2], \
                   'last_year_revenue':Result[0][3], 'One_year_Target': Result[0][4]}
            #print("Search Result", Res)
            return Res
        else:
            return -1
    #End Search

'''
d =Populate()
d.Insert_Database('74.5', '47,004,057,441','123', '1,311,000', 'OXY')

d =Populate()
d.Search("oxy")
'''