from django.urls import path
from django.views.generic import TemplateView
from . import views


urlpatterns = [
    path('', views.Index, name='index'),
    path('news/<str:sym>', views.News, name='news'),
    path('transcript/<str:sym>', views.Transcript, name='transcript'),
    path("newsdetails/<str:id>", views.Detail_News, name="detail_news"),
    path("transcriptdetails/<str:id>", views.Detail_Transcript, name="detail_transcript"),
]