# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

'''
To create model out of legacy database:
use this command:
    python manage.py inspectdb > models.py
'''

from django.db import models


class CompanyDetails(models.Model):
    company_name = models.CharField(db_column='Company_Name', max_length=20)  # Field name made lowercase.
    market_cap = models.CharField(max_length=20)
    net_cash_flow = models.IntegerField()
    last_revenue = models.IntegerField()
    future = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'company_details'
