'''
Author: Meet Shah(a.k.a slim_shah)

Source:
        Querying Models and Saving Data into database: https://docs.djangoproject.com/en/dev/topics/db/queries/#creating-objects
'''

from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.conf import settings
from django.views import generic
from .models import CompanyDetails
# Create your views here.
import sys
sys.path.insert(0, "D:\\Project\\Nesh\\Database")
from Insert import Populate
from Elastic import ElasticSearch


def Index(request):
    Query = Populate()
    if request.method == 'POST':
        Search_Company = request.POST.get('search')

        if Search_Company == "":
            context = {'Flag':1}
            return render(request, 'nasdaq/index.html', context)

        result = Query.Search(Search_Company)
        context = {}
        if result!= -1:
            context ={'Result': result, 'Flag':1}
        else:
            result= {'sym':'#'}
            context = {'Result': result, 'Flag': 0}
        return render(request, 'nasdaq/Result.html', context)

    return render(request, 'nasdaq/index.html')

def News(request, sym):
    es = ElasticSearch()
    Headlines = es.Search_news(sym, "news")
    context = {'Headlines' : Headlines, 'sym':sym}
    return render(request, 'nasdaq/news.html' ,context)

def Transcript(request, sym):
    es = ElasticSearch()
    transcripts = es.Search_transcript(sym, "transcripts")
    context = {'Transcripts': transcripts, 'sym':sym}
    return render(request, 'nasdaq/transcripts.html', context)

def Detail_News(request, id):
    es = ElasticSearch()
    News = es.Detail_News(id, "news")
    text = News['article_text']
    text = text.split('\n')
    Line =[]
    for i in text:
        Line.append(i)
    News['article_text'] = Line
    return render(request, 'nasdaq/DetailNews.html', News)

def Detail_Transcript(request, id):
    es = ElasticSearch()
    transcripts = es.Detail_Transcript(id, "transcripts")
    text = transcripts['content']
    text = text.split('\n')
    Line = []
    for i in text:
        Line.append(i)
    transcripts['content'] = Line
    return render(request, 'nasdaq/DetailTranscript.html', transcripts)




