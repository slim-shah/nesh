# Nesh
This project scraps public stock website nasdaq and stores it's information into database and provides access to that inforamtion via interactive graphical interface by building web-portal in Django.

This project has three main modules.
   1. Crawler.
   2. Database.
   3. Web Portal.

# Project-Demo and Explanation 

https://www.youtube.com/watch?v=TcWDweejtDY&t=611s

# Module and its function

| Module Name|  Techonlogy |Function |
| :-:        |  :-:  |:-:       |
| Crawler    |  Selenium + Beautiful Soup |Contains code for crawling 'nasdaq.com' from the given input seed value of company symbols.|
| Database   | MySql + Elasticsearch |Contains code for storing and fetching data in mysql database and elasticsearch.|
|Web Portal| Django| Provides interactice Gui for visualizing stored data.|
# Dependencies
Here is the list of dependencies that you will need to install on your computer for running this code on your machine.

   | Sr.No | Dependency  | version  |
   | :---: | :-: | :-: |
   | 1. | Python |  3.7.2|
   | 2. | Msql | 5.6.17  |
   | 3. |  ElasticSearch | 6.2 |
   | 5. | Chrome Driver | - |

# Library Used
| Sr.No | Name | Version |
| :---: | :-: | :-: |
| 1. | bs4 |  0.0.1|
| 3. |  Django | 2.1.7 |
| 4. | Mysql-Connector-python | 8.0.15|
| 5. |  elasticsearch | 6.3.1 |
| 6. |  lxml | 4.3.0 |
| 7. | requests | 2.21.0 |
| 8. | selenium | 3.141.0|

# Database Schema
For running this code first you will have to create database named 'nesh' in your local machine and then create relation 'company_details' with following schema.

|Field| Type|
|:-:| :-:| 
|Company_Name| varchar(20)|
|market_cap| varchar(20)|
|net_cash_flow| int(11)|
|last_revenue| int(11)|
|future| int(11) |


# Getting Started
Clone this repo on your local machine and follow the further instruction.

### Running the Web-Crawler:
For running web crawler navigate inside Web-Crawler folder and locate main.py and type in following command 0n your terminal.

    $python main.py

### Running Web-Portal:
Navigate to nesh directory and locate manage.py file. After that type in following command on your cmd.

    $python manage.py runserver:desired_port number

Once the server starts, open the web browser on your machine and type in following address to open index page.

    http://127.0.0.1:8000/nasdaq/

**Note**: In the file *'Repository-Root/Database/Elastic.py'* I have hard coded my credentials for elastic search so make changes accordingly to your needs.

Same things goes for *'Repository-Root/Database/Insert.py'* file.

# Architecture

Following architecture gives the brief overview of our project. To get more detail information
![](/ScreenShots/Architecture.jpg?raw=true "Optional Title")

# ScreenShots
### Home Page
![](/ScreenShots/home.jpg?raw=true "Optional Title")

### Search Result
![](/ScreenShots/searchResult.jpg?raw=true "Optional Title")

### News
![](/ScreenShots/News.jpg?raw=true "Optional Title")

### News_Detail
![](/ScreenShots/newsDetail.jpg?raw=true "Optional Title")

### Transcript
![](/ScreenShots/Transcrpt.jpg?raw=true "Optional Title")

### Transcript_Detail
![](/ScreenShots/transcriptDetail.jpg?raw=true "Optional Title")

### Database
![](/ScreenShots/Database.jpg?raw=true "Optional Title")

### ElasticSearch
![](/ScreenShots/ElasticSearch.jpg?raw=true "Optional Title")