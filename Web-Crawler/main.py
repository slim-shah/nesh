'''
Author: Meet Shah (a.k.a slim_shah)
Date: 03/08/2019
References:
'''
#importing custom crawler class
from Scrapper import Scrap


if __name__ == '__main__':
    scrap = Scrap()
    SYM = []
    #Opening the text file which contains the company symbols for which information is need to parse.
    with open('company_list.txt', 'rt') as file:
        for sym in file:
            sym = sym.replace('\n', '')
            SYM.append(sym.lower())

    for sym in SYM:
        print("Currently scrapping:", sym)
        #scrap.parse(sym)
        scrap.Parse_Transcript(sym)
        #scrap.Parse_News(sym)
        print("Finished scrapping:", sym)
    print("Finished Scrapping !!!")
#End Main
