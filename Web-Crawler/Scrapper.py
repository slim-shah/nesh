'''
Author: Meet Shah
Date: 03/08/2019
'''
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import os
from bs4 import BeautifulSoup
import sys
import requests
import time
import random

'''
importing custom made modules for storing data.
'''
sys.path.insert(0, "D:\\Project\\Nesh\\Database")
from Insert import Populate
from Elastic import ElasticSearch

class Scrap:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    '''
    Default constrcutor of the class
    '''
    def __init__(self):
        #Base URL
        self.Base_URL = "https://www.nasdaq.com/"

        chrome_driver = "D:\\Joseph\\chromedriver.exe"
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--ignore-ssl-errors')
        self.driver = webdriver.Chrome(chrome_driver, chrome_options=options)

        self.Database = Populate()

        self.elastic = ElasticSearch()
    #End of init

    '''
    This method will close the browser opened by selenium chrome driver.
    '''
    def close_browser(self):
        try:
            self.driver.close()
        except:
            pass

    '''
    This method accepts company symbol and creates url out of it to fetch some of the key insight like market capital, one year target etc..
    input: company symbol eg:(oxy, eog, apc etc)
    output: revelant information is stored into msql database (table_name: company_details).
    '''
    def parse(self, sym):
        print("inside parse")
        url = "https://www.nasdaq.com/symbol/" + sym
        #driver = webdriver.Chrome(self.chrome_driver, desired_capabilities=capa)

        self.driver.maximize_window()
        self.driver.get(url)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        one_year_target = "//*[@id='left-column-div']/div[1]/div[1]/div/div[1]/div[2]"
        S_E = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.XPATH, one_year_target)))
        #S_E = driver.find_element_by_xpath(one_year_target)
        one_year_target = S_E.get_attribute('innerHTML')
        one_year_target = str(one_year_target).replace(' ', '')

        Market_Cap = "//*[@id='left-column-div']/div[1]/div[1]/div/div[7]/div[2]"
        S_E = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, Market_Cap)))
        #S_E = driver.find_element_by_xpath(Market_Cap)
        Market_Cap = S_E.get_attribute('innerHTML')
        Market_Cap = str(Market_Cap).replace(' ', '')


        url = "https://www.nasdaq.com/symbol/" + sym + "/financials?query=cash-flow"
        self.driver.get(url)
        #driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        #print("currently on this url", url)
        net_cash_flow = "//*[@id='financials-iframe-wrap']/div[1]/table/tbody/tr[22]/td[2]"
        S_E = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, net_cash_flow)))
        #S_E = driver.find_element_by_xpath(net_cash_flow)
        net_cash_flow = S_E.get_attribute('innerHTML')


        url = "https://www.nasdaq.com/symbol/" + sym + "/financials?query=income-statement"
        self.driver.get(url)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        #print("currently on this url", url)

        last_year_revenue = "//*[@id='financials-iframe-wrap']/div[1]/table/tbody/tr[18]/td[3]"
        #S_E = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, last_year_revenue)))
        S_E = self.driver.find_element_by_xpath(last_year_revenue)
        last_year_revenue = S_E.get_attribute('innerHTML')
        last_year_revenue = last_year_revenue.replace('$', '')

        self.Database.Insert_Database(one_year_target, Market_Cap, net_cash_flow, last_year_revenue, sym)
        self.close_browser()
    #end parse

    '''
    This method crawls and fetches the transcript of company symbol (OXY, APC etc) and stores the information in
    Elastic Search.
    input: company symbol eg:(oxy, eog, apc etc)
    output: Information is stored in ElasticSearch.
    '''
    def Parse_Transcript(self, sym):

        url = self.Base_URL + "symbol/" + sym + "/call-transcripts"
        print("Currently crawling:", url)
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "lxml")

        table_id = "quotes_content_left_CalltranscriptsId_CallTranscripts"
        table = soup.find("table", {"id": table_id })
        AnchorTags = table.findChildren("a")

        for a in AnchorTags:
            try:
                st = a['href']
                if "javascript" in st:
                    continue
                self.Fetch_details( self.Base_URL + a['href'], sym)
            except:
                continue
            time.sleep(random.randint(5,10))
    #End Parse_Transcript


    '''
    This method crawls and fetches the news of company symbol (OXY, APC etc) and stores the information in
    Elastic Search.
    input: company symbol eg:(oxy, eog, apc etc)
    output: Information is stored in ElasticSearch.
    '''
    def Parse_News(self, sym):
        url = self.Base_URL + "symbol/" + sym + "/news-headlines"
        flag = 1
        count = 0
        a = ''
        while a!= None and count<20:
            r = requests.get(url)
            soup = BeautifulSoup(r.text, "lxml")

            form_id = "Companiesnews"
            form = soup.find("form", {"id": form_id})
            span = form.findAll("span", {"class": "fontS14px"})
            for s in span:
                a = s.find("a")
                self.Fetch_detail_news(a['href'])

            next_id = "quotes_content_left_lb_NextPage"
            a = soup.find("a", {"id": next_id})
            if a!= None:
                url = a['href']
            else:
                break
            count += 1
        print("news website crawled", count)
    #End Parse_News


    '''
    Helper method for Parse_News(self, sym) method.
    input: full url
    output: parse information like, article_header, article_text, date, by and store it in elastic search.
    '''
    def Fetch_detail_news(self, url):
        try:
            r = requests.get(url)
            soup = BeautifulSoup(r.text, "lxml")
            article_header = soup.find("h1", {"class": "article-header"})
            article_header = article_header.get_text()

            div = soup.find("div", {"class": "article-byline"})
            span = div.findAll("span")
            date = ''
            by = ''
            for i in range(0, len(span)):
                if i == 0:
                    date = span[i].get_text()
                elif i==1:
                    by = span[i].get_text()

            article_div = soup.find("div", {"id": "articleText"})
            article_text = ''
            article_text = article_div.getText()
            self.elastic.Ingest_news(article_header, date, by, article_text)

        except Exception as E:
            print(url)
            print(E)
    #End Fetchdetails

    '''
    Helper method for Parse_Transcript(self, sym) method.
    input: full url
    output: parse information like transcript_text and store it in elastic search.
    '''

    def Fetch_details(self, url, sym):
        #print(url)
        r = requests.get(url)
        soup = BeautifulSoup(r.text, "lxml")

        div_id = "SAarticle"
        div_id = soup.find("div", {"id": div_id})
        p = div_id.findChildren("p")

        st = ''
        try:
            for i in p:
                st = st + i.getText() + '\n'
                #print(i.getText())

            self.elastic.Ingest( sym, st, 'transcripts', 'transcript')

        except Exception as E:
            print(E)
    #End Fetch_details